## Objectives

1. Students will review String manipulation using Ruby methods
2. Students should familiarize themselves with test driven development
3. Students will learn about arrays and hashes, and how to iterate over them


## Resources

### Test Driven Development

* [Intro to TDD, RSpec, and Learn](https://github.com/learn-co-curriculum/intro-to-tdd-rspec-and-learn)
* [Test First Teaching](http://testfirst.org/about#tdd)
* [Wikipedia's take on TDD](http://en.wikipedia.org/wiki/Test-driven_development)
* [An Introduction to TDD](http://agiledata.org/essays/tdd.html)
* [Rspec documentation](https://relishapp.com/rspec)
* [More RSpec](http://blog.teamtreehouse.com/an-introduction-to-rspec)

### Ruby Data Structures and Iterations

* [Basic Loops without Data Structures](https://github.com/learn-co-curriculum/looping-readme)
* [Ruby Doc for Arrays](https://ruby-doc.org/core-2.2.0/Array.html)
* [Creating, Reading, Updating and Deleting with Arrays](https://github.com/learn-co-curriculum/array-readme)
* [Enumerables Readme](https://github.com/learn-co-curriculum/enumerators-readme)
* [Intro to Hashes](https://github.com/learn-co-curriculum/hash-overview-readme)
* [Video - Hashes](https://www.youtube.com/embed/0JSsFQGYaeA)
* [Ruby Doc for Hashes](https://ruby-doc.org/core-2.5.0/Hash.html)
* [Iterations Video](https://www.youtube.com/watch?v=_enCfTEZZ6Y)
